## Javascript Basics

This code demonstrates the basic syntax for the Javascript programming language.

---

## How to download the files

### OPTION 1: Download from your browser

Click on `Downloads` > `Download Repository`

### OPTION 2: Using some kind of git software

In your git software, clone the repository

Clone command:

`git clone https://jenelleteaches@bitbucket.org/jenelleteaches/webscripting-javascriptexamples.git`

---

## How to run

### OPTION 1: Using repl.it

1. Copy and paste the `basics.js` code into [http://repl.it/languages/javascript](http://repl.it/languages/javascript)
2. Press play!

### OPTION 2: Running the code in a browser

1. Download `index.html` and `basics.js` --> HINT: it's in the [code](https://bitbucket.org/jenelleteaches/webscripting-javascriptexamples/src/master/code/) `code` folder!
2. Open `index.html` in Google Chrome
3. In Google Chrome, open Developer Tools (Press `F12` on your keyboard)
4. In `Developer Tools`, open the `Console` tab
5. Look at the output

To modify the Javsacript code:

* Make your changes to `basics.js`
* In Chrome, go to your `index.html` tab
* Press REFRESH
* Look at the output in the `Developer Tools` > `Console`
